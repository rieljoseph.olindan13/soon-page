<?php

use PHPMailer\PHPMailer\PHPMailer;

require 'phpmailer/src/PHPMailer.php';

// If you intend you use SMTP, uncomment next line
require 'phpmailer/src/SMTP.php';

$toemails = array();

$toemails[] = array(
	'email' => 'info@coffeeorcode.com', // Your Email Address
	'name' => 'Coffee or Code Inc.' // Your Name
);

// Form Processing Messages
$message_success = 'We have successfully received your Message and will get Back to you as soon as possible.';

$mail = new PHPMailer();
$autoresponder = new PHPMailer();

// If you intend you use SMTP, add your SMTP Code after this Line

$mail->IsSMTP();
$mail->Host = "smtp.gmail.com";
$mail->Mailer = "smtp";
$mail->SMTPDebug = 2;
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'ssl';
$mail->Port = 465;
$mail->Username = "info@coffeeorcode.com";
$mail->Password = "infoC0ffeeorcode!";

$autoresponder->IsSMTP();
$autoresponder->Host = "smtp.gmail.com";
$autoresponder->Mailer = "smtp";
$autoresponder->SMTPDebug = 2;
$autoresponder->SMTPAuth = true;	
$autoresponder->SMTPSecure = 'ssl';
$autoresponder->Port = 465;
$autoresponder->Username = "info@coffeeorcode.com";
$autoresponder->Password = "infoC0ffeeorcode!";

if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	if( $_POST['cf-email'] != '' ) {

		$name = isset( $_POST['cf-name'] ) ? $_POST['cf-name'] : '';
		$email = isset( $_POST['cf-email'] ) ? $_POST['cf-email'] : '';
		$subject = 'New Message From Contact Form';
		$message = isset( $_POST['cf-message'] ) ? $_POST['cf-message'] : '';

		$botcheck = $_POST['cf-botcheck'];

		if( $botcheck == '' ) {

			$mail->CharSet = 'UTF-8';
			$mail->SetFrom( $email , $name );
			$mail->AddReplyTo( $email , $name );
			foreach( $toemails as $toemail ) {
				$mail->AddAddress( $toemail['email'] , $toemail['name'] );
			}

			$autoresponder->SetFrom( "noreply@coffeeorcode.com" );
			$autoresponder->AddReplyTo( "noreply@coffeeorcode.com" );
			$autoresponder->AddAddress( $email , $name );
			$autoresponder->Subject = "Your email has been received";
			$ar_body = "Thank you for contacting us!<br>We will get back to you as soon as we can.";

			$mail->Subject = $subject;

			$name = isset($name) ? "Name: $name<br><br>" : '';
			$email = isset($email) ? "Email: $email<br><br>" : '';
			$message = isset($message) ? "Message: $message<br><br>" : '';

			$referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This form was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';

			$body = "$name $email $message $referrer";

			$autoresponder->MsgHTML( $ar_body );
			$mail->MsgHTML( $body );
			
			$sendEmail = $mail->Send();

			if( $sendEmail == true ){
				$send_arEmail = $autoresponder->Send();
				if($send_arEmail == true){
					echo '{ alert: "success", message: "' . $message_success . '" }';
				}else{
					echo '{ alert: "error", message: "' . $autoresponder->ErrorInfo . '" }';
				}
					
			}else{
				echo '{ alert: "error", message: "Email could not be sent due to some Unexpected Error. Please Try Again later.<br /><br />Reason:<br />' . $mail->ErrorInfo . '" }';
			}
		} else {
			echo '{ alert: "error", message: "Bot Detected.! Clean yourself Botster.!" }';
		}
	} else {
		echo '{ alert: "error", message: "Please Fill up all the Fields and Try Again." }';
	}
} else {
	echo '{ alert: "error", message: "An unexpected error occured. Please Try Again later." }';
}

?>